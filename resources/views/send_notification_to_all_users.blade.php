@extends('voyager::master')
@section('content')
    <h1 class="page-title"><i class="voyager-mail"></i> Send Notifications</h1><br>
    <div class="panel-body" style="background-color: white;">
        <h4>Send Notification to All Users</h4>
        <form method="POST" action="http://marouf.leatherworks.giraffecode.com/public/api/sendNotification">
            {{ @csrf_field() }} 
            <fieldset>
                <div class="container">
                    <label>Notification title</label>
                    <input type="text" placeholder="Title" class="form-control" name="headings" required>   <br>
                    <label>Notification content</label>
                    <input type="text" placeholder="Content" class="form-control" name="contents" required>   <br>
                    <button type="submit" style="font-weight: bold;" class="btn btn-success">Send</button>
                </div>
            </fieldset>
        </form>
    </div>
    <hr>
    <div class="panel-body" style="background-color: white;">
        <h4>Send Notification to Specific User</h4>
        <form method="POST" action="http://marouf.leatherworks.giraffecode.com/public/api/sendNotificationtoUser">
            {{ @csrf_field() }} 
            <fieldset>
                <div class="container">
                    <label>Select User</label>
                    <select name="user_id" class="form-control" required>
                        <option value="" disabled selected>User</option>
                        @foreach($users as  $key => $value)
                            <option value="{{ $value['id'] }}">{{ $value['email'] }}</option>
                        @endforeach 
                    </select><br>
                    <label>Notification title</label>
                    <input type="text" placeholder="Title" class="form-control" name="headings" required><br>
                    <label>Notification content</label>
                    <input type="text" placeholder="Content" class="form-control" name="contents" required><br>
                    <button type="submit" style="font-weight: bold;" class="btn btn-success">Send</button>
                </div>
            </fieldset>
        </form>
    </div>
@endsection