<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//user
Route::post('/register', 'UserController@Register');
Route::post('/login', 'UserController@Login');
Route::get('/activeUser/{email}', 'UserController@ActiveUser');
Route::post('/checkEmail', 'UserController@CheckEmail');
Route::post('/updateUserDate', 'UserController@UpdateUserDate');
Route::post('/resetPassword/{email}', 'UserController@ResetPassword');
Route::get('/resendMailforResetPassword/{email}', 'UserController@ResendMailforResetPassword');
Route::get('/resendConformationEmail/{email}', 'UserController@ResendConformationEmail');
Route::get('/addtoCartCheck/{user_id}', 'UserController@addtoCartCheck');
Route::get('/listUsers', 'UserController@ListUsers');
Route::get('/listNormalUsers', 'UserController@ListNormalUsers');
Route::post('/registerByFacebook', 'UserController@RegisterByFacebook');
Route::post('/loginByFacebook', 'UserController@LoginByFacebook');
Route::post('/registerByGoogle', 'UserController@RegisterByGoogle');
Route::post('/loginByGoogle', 'UserController@LoginByGoogle');
//brandSetting
Route::get('/getBrandSetting', 'UserController@GetBrandSetting');
Route::get('/listBrandSetting', 'UserController@ListBrandSetting');
Route::post('/updateBrandSetting', 'UserController@UpdateBrandSetting');
Route::post('/createAdmin', 'UserController@CreateAdmin');
Route::post('/loginAdmin', 'UserController@LoginAdmin');

//notification
Route::post('/sendNotification', 'UserController@SendNotification');
Route::post('/sendNotificationtoUser', 'UserController@SendNotificationtoUser');

//category
Route::get('/listCategory', 'CategoryController@ListCategory');
Route::get('/listCategoryDashboard', 'CategoryController@ListCategoryDashboard');
Route::post('/createCategory', 'CategoryController@CreateCategory');
Route::get('/deleteCategory/{category_id}', 'CategoryController@DeleteCategory');
Route::get('/editCategory/{category_id}', 'CategoryController@EditCategory');
Route::post('/updateCategory/{category_id}', 'CategoryController@UpdateCategory');
Route::post('/bulkDeleteCategory', 'CategoryController@BulkDeleteCategory');

//products
Route::get('/listFeatureProduct', 'ProductController@ListFeatureProduct');
Route::get('/getProducts', 'ProductController@GetProducts');
Route::get('/listProducts', 'ProductController@ListProducts');
Route::get('/listReviews', 'ProductController@ListReviews');
Route::post('/saveReview', 'ProductController@SaveReview');
Route::get('/listProductAttachedtoCategory/{category_id}', 'ProductController@ListProductAttachedToCategory');
Route::post('/createProduct', 'ProductController@CreateProduct');
Route::get('/productDetails/{product_id}', 'ProductController@ProductDetails');
Route::post('/updateProduct/{product_id}', 'ProductController@UpdateProduct');
Route::get('/deleteProduct/{product_id}', 'ProductController@DeleteProduct');
Route::post('/bulkDeleteProduct', 'ProductController@BulkDeleteProduct');

//order
Route::post('/createOrder', 'OrderController@CreateOrder');
Route::get('/getOrders/{user_id}', 'OrderController@GetOrders');
Route::get('/listOrders', 'OrderController@ListOrders');

//deliveryfees
Route::get('/listDeliveryFees', 'OrderController@ListDeliveryFees');
Route::get('/listDeliveryFeesDashboard', 'OrderController@ListDeliveryFeesDashboard');
Route::post('/createDeliveryFees', 'OrderController@CreateDeliveryFees');
Route::get('/editDeliveryFees/{deliveryfees_id}', 'OrderController@EditDeliveryFees');
Route::post('/updateDeliveryFees/{deliveryfees_id}', 'OrderController@UpdateDeliveryFees');
Route::get('/deleteDeliveryFees/{deliveryfees_id}', 'OrderController@DeleteDeliveryFees');
Route::post('/bulkDeleteDeliveryFees', 'OrderController@BulkDeleteDeliveryFees');

//promocode
Route::post('/checkPromoCode', 'PromoCodeController@CheckPromoCode');
Route::post('/createPromoCode', 'PromoCodeController@CreatePromoCode');
Route::get('/listPromoCode', 'PromoCodeController@ListPromoCode');
Route::get('/editPromoCode/{promocode_id}', 'PromoCodeController@EditPromoCode');
Route::post('/updatePromoCode/{promocode_id}', 'PromoCodeController@UpdatePromoCode');
Route::get('/deletePromoCode/{promocode_id}', 'PromoCodeController@DeletePromoCode');
Route::post('/bulkDeletePromoCode', 'PromoCodeController@BulkDeletePromoCode');