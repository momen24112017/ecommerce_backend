<?php

namespace App;
use App\Status;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_category_id', 'name', 'price', 'quantity', 'fake_quantity', 'images','second_image',
        'third_image', 'long_desc', 'short_desc', 'is_feature'
    ];

    public function listProductAttachtoCategory($category_id){
        $App_URL_MEDIA = env('App_Media_URL');
        $arrProduct = $this->where('product_category_id', $category_id)->paginate(3);
        foreach($arrProduct as $images=>$value){
            $value['images'] = $App_URL_MEDIA.$value['images'];
        }
        return $arrProduct;
    }

    public function relatedProduct($category_id){
        $App_URL_MEDIA = env('App_Media_URL');
        $arrProduct = $this->where('product_category_id', $category_id)->get();
        foreach($arrProduct as $images=>$value){
            $value['images'] = $App_URL_MEDIA.$value['images'];
        }
        return $arrProduct;
    }

    public function productDetails($product_id){
        $App_URL_MEDIA = env('App_Media_URL');
        $product = $this->where('id', $product_id)->get();
        foreach($product as $images=>$value){
            $value['images'] = $App_URL_MEDIA.$value['images'];
            $value['second_image'] = $App_URL_MEDIA.$value['second_image'];
            $value['third_image'] = $App_URL_MEDIA.$value['third_image'];
        }
        return $product;
    }

    public function listFeatureProduct(){
        $App_URL_MEDIA = env('App_Media_URL');
        $arrProduct = $this->where('is_feature', 1)->get();
        foreach($arrProduct as $images=>$value){
            $value['images'] = $App_URL_MEDIA.$value['images'];
        }
        return $arrProduct;
    }

    public function updateProductQuantity($arrProduct){
        foreach($arrProduct as $key){
            $productQuantity = $this->select('quantity')->where('id', $key['product_id'])->get('quantity');
            $newQuantity = $productQuantity - $key['quantity'];
            $updatedQuantity = $this->where('id', $key['product_id'])->update(array('quantity' => $newQuantity));
            return $updatedQuantity;
        }
    }

    public function getProductName($product_id){
        $productName= $this->where('id', $product_id)->get('name');
        return $productName;
    }

    public function getProductById($product_id){
        $productName= $this->where('id', $product_id)->get();
        return $productName;
    }

    public function checkProductQuantity($arrProduct){
        $arr = array();
        foreach($arrProduct as $key){
            $product = $this->select('quantity')->where('id', $key['id'])->first();
            return $product;
        }
    }

    public function getProducts(){
        $products = $this->all();
        return $products;
    }

    public function listProducts(){
        $App_URL_MEDIA = env('App_Media_URL');
        $products = $this->all();
        foreach($products as $image=>$value){
            $value['images'] = $App_URL_MEDIA.$value['images'];
            if($value['second_image'] != "NULL" && $value['second_image'] != "null" && $value['second_image'] != null){
                $value['second_image'] = $App_URL_MEDIA.$value['second_image'];
            }
            if($value['third_image'] != "NULL" && $value['third_image'] != "null" && $value['third_image'] != null){
                $value['third_image'] = $App_URL_MEDIA.$value['third_image'];
            }
        }
        return $products;
    }

    public function createProduct($input){
        if($input['images'] != "null"){
            $image = $input['images'];
            $imageName = $image->getClientOriginalName();
            $image->move('storage/products', $imageName);
            $input['images'] = "products/".$imageName;
        }
        if($input['second_image'] != "null"){
            $image = $input['second_image'];
            $imageName = $image->getClientOriginalName();
            $image->move('storage/products', $imageName);
            $input['second_image'] = "products/".$imageName;
        }
        if($input['third_image'] != "null"){
            $image = $input['third_image'];
            $imageName = $image->getClientOriginalName();
            $image->move('storage/products', $imageName);
            $input['third_image'] = "products/".$imageName;
        }
        $product= $this->create($input);
        return $product;
    }

    public function updateProduct($input, $product_id){
        if(array_key_exists ('images', $input)){
            $image = $input['images'];
            $imageName = $image->getClientOriginalName();
            $image->move('storage/products', $imageName);
            $input['images'] = "products/".$imageName;
        }
        if(array_key_exists ('second_image', $input)){
            $image = $input['second_image'];
            $imageName = $image->getClientOriginalName();
            $image->move('storage/products', $imageName);
            $input['second_image'] = "products/".$imageName;
        }
        if(array_key_exists ('third_image', $input)){
            $image = $input['third_image'];
            $imageName = $image->getClientOriginalName();
            $image->move('storage/products', $imageName);
            $input['third_image'] = "products/".$imageName;
        }
        $product = $this->where('id', $product_id)->update($input);
        return $product;
    }

    public function deleteProduct($product_id){
        $product = $this->where('id', $product_id)->delete();
        return $product;
    }

    public function bulkDeleteProduct($products){
        foreach($products as $product_id){
            $product = $this->where('id', $product_id)->delete();
        }
        return $product;
    }

}
