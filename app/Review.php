<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'user_id', 'product_id', 'review', 'rate'
    ];

    public function GetProductReviews($product_id){
        $reviews = $this->where('product_id', $product_id)->orderBy('created_at', 'desc')->get();
        return $reviews;
    }

    public function saveReview($input){
        $data = array(
            'user_id'    => $input['user_id'],
            'product_id' => $input['product_id'],
            'review'     => $input['review'],
            'rate'       => $input['rate']
        );
        $create = Review::create($data);
    }

    public function listReviews(){
        $reviews = $this->get();
        return $reviews;
    }

}
