<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{   
    
    protected $fillable = [
        'name', 'image'
    ];
    
    public function listCategory(){
        $App_URL_MEDIA = env('App_Media_URL');
        $arrCategory = $this->all();
        foreach($arrCategory as $image=>$value){
            $value['image'] = $App_URL_MEDIA.$value['image'];
        }
        $arrCategory = $arrCategory->toArray();
        return $arrCategory;
    }

    public function listCategoryDashboard(){
        $App_URL_MEDIA = env('App_Media_URL');
        $arrCategory = $this->paginate(10);
        foreach($arrCategory as $image=>$value){
            $value['image'] = $App_URL_MEDIA.$value['image'];
        }
        $arrCategory = $arrCategory->toArray();
        return $arrCategory;
    }
   
    public function getCategoryName($category_id){
        $result = $this->where('id', $category_id)->get('name');
        return $result;
    }

    public function getCategorybyId($category_id){
        $result = $this->where('id', $category_id)->get();
        return $result;
    }

    public function getCategoryImage($category_id){
        $App_URL_MEDIA = env('App_Media_URL');
        $category_image = $this->where('id', $category_id)->first();
        $category_image = $App_URL_MEDIA.$category_image->image;
        return $category_image;
    }

    public function deleteCategory($category_id){
        $category = $this->where('id', $category_id)->delete();
        return $category;
    }

    public function editCategory($category_id){
        $App_URL_MEDIA = env('App_Media_URL');
        $category = $this->where('id', $category_id)->get();
        $category[0]['image'] = $App_URL_MEDIA.$category[0]['image'];
        return $category;
    }

    public function updateCategory($category, $category_id){
        if(array_key_exists ('image', $category)){
            $image = $category['image'];
            $imageName = $image->getClientOriginalName();
            $image->move('storage/product-categories', $imageName);
            $category['image'] = "product-categories/".$imageName;
        }
        $category = $this->where('id', $category_id)->update($category);
        return $category;
    }

    public function bulkDeleteCategory($categories){
        foreach($categories as $category_id){
            $category = $this->where('id', $category_id)->delete();
        }
        return $category;
    }

}
