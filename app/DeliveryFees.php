<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryFees extends Model
{
    protected $fillable = [
        'city', 'price'
    ];

    public function getDeliveryFees($city){
        $objDeliveryFess= $this->where('city', $city)->first();
        return $objDeliveryFess;
    }

    public function listDeliveryFees(){
        $arrDelivery = $this->all();
        return $arrDelivery;
    }

    public function listDeliveryFeesDashboard(){
        $arrDelivery = $this->paginate(10);
        return $arrDelivery;
    }

    public function editDeliveryFees($deliveryfees_id){
        $deliveryfees = $this->where('id', $deliveryfees_id)->get();
        return $deliveryfees;
    }

    public function updateDeliveryFees($deliveryfees, $deliveryfees_id){
        $deliveryfees = $this->where('id', $deliveryfees_id)->update($deliveryfees);
        return $deliveryfees;
    }

    public function deleteDeliveryFees($deliveryfees_id){
        $deliveryfees = $this->where('id', $deliveryfees_id)->delete();
        return $deliveryfees;
    }

    public function bulkDeleteDeliveryFees($deliveryfees){  
        foreach($deliveryfees as $deliveryfees_id){
            $deliveryfee = $this->where('id', $deliveryfees_id)->delete();
        }
        return $deliveryfee;
    }

}
