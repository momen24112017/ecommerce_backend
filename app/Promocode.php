<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    protected $fillable = [
        'promocode', 'value', 'valid_date'
    ];

    public function checkPromocode($promocode){
        $dateNow = now();
        $promoCode = $this->where('promocode', $promocode)->where('valid_date','>',$dateNow)->get();
        return $promoCode;
    }

    public function promocodeValue($promocode){
        $value = $this->where('promocode', $promocode)->get('value');
        return $value;
    }

    public function getPromocodebyPromoCode($promocode){
        $id = $this->where('promocode', $promocode)->first();
        return $id;
    }

    public function getPromocodebyId($promocode_id){
        $id = $this->where('id', $promocode_id)->first();
        return $id;
    }

    public function createPromocode($promocode){
        $promocode = $this->create($promocode);
        return $promocode;
    }

    public function getPromocodes(){
        $promocode = $this->get()->toArray();
        return $promocode;
    }

    public function updatePromocode($promocode_id, $promocode){
        $promocode = $this->where('id', $promocode_id)->update($promocode);
        return $promocode;
    }

    public function deletePromocode($promocode_id){
        $promocode = $this->where('id', $promocode_id)->delete();
        return $promocode;
    }

    public function bulkDeletePromoCode($promocodes){
        foreach($promocodes as $promocode_id){
            $promocode = $this->where('id', $promocode_id)->delete();
        }
        return $promocode;
    }

}
