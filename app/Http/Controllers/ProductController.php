<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use App\Review;
use App\User;
use App\Status;

class ProductController extends Controller
{
    public function ListProductAttachedtoCategory($category_id){
        $arr = array();
        $objProduct = new Product();
        $objProductCategory = new ProductCategory();
        $Product = $objProduct->listProductAttachtoCategory($category_id);
        $categoryName = $objProductCategory->getCategoryName($category_id);
        $categoryImage = $objProductCategory->getCategoryImage($category_id);
        $arr['categoryName'] = $categoryName;
        $arr['categoryImage'] = $categoryImage;
        $arr['data'] = $Product;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ProductDetails($product_id){
        $arr = array();
        $objProduct = new Product();
        $product_details = $objProduct->productDetails($product_id);
        $category_id = $product_details[0]['product_category_id']; 
        $related_product = $objProduct->relatedProduct($category_id);
        $objReview = new Review();
        $product_review = $objReview->GetProductReviews($product_id);
        $objUser = new User();
        foreach($product_review as $review){
            $user = $objUser->getUserById($review['user_id']);
            $review['user_id'] =  $user['name'];
        }
        $objProductCategory = new ProductCategory();
        $category = $objProductCategory->getCategorybyId($category_id);
        $arr['data']['product_category'] = $category;
        $arr['data']['product_review'] = $product_review;
        $arr['data']['product_details'] = $product_details;
        $arr['data']['related_product'] = $related_product;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListFeatureProduct(){
        $arr = array();
        $objProduct = new Product();
        $featurProduct = $objProduct->listFeatureProduct();
        $arr['data'] = $featurProduct;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function GetProducts(){
        $arr = array();
        $objProduct = new Product();
        $products = $objProduct->getProducts();
        $arr['data'] = $products;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListProducts(){
        $arr = array();
        $objProduct = new Product();
        $products = $objProduct->listProducts();
        $objProductCategory = new ProductCategory();
        for($i = 0; $i < count($products); $i++){
            $category = $objProductCategory->getCategorybyId($products[$i]['product_category_id']);
            $orders[$i]['product_category_id']= $category;
        }
        $arr['data'] = $products;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListReviews(){
        $arr = array();
        $objReview = new Review();
        $reviews = $objReview->listReviews();
        $objUser = new User();
        $objProduct = new Product();
        for($i = 0; $i < count($reviews); $i++){
            $user = $objUser->getUserById($reviews[$i]['user_id']);
            $reviews[$i]['user_id'] = $user;
            $product = $objProduct->getProductById($reviews[$i]['product_id']);
            $reviews[$i]['product_id']= $product;
        }
        $arr['data'] = $reviews;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function SaveReview(Request $request){
        $arr = array();
        $input = $request->all();
        $objReview = new Review();
        $products = $objReview->saveReview($input);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function CreateProduct(Request $request){
        $arr = array();
        $input = $request->all();
        $objProduct = new Product();
        $products = $objProduct->createProduct($input);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function UpdateProduct(Request $request, $product_id){
        $arr = array();
        $input = $request->all();
        $objProduct = new Product();
        $products = $objProduct->updateProduct($input, $product_id);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function DeleteProduct($product_id){
        $arr = array();
        $objProduct = new Product();
        $products = $objProduct->deleteProduct($product_id);
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function BulkDeleteProduct(Request $request){
        $arr = array();
        $input = $request->all();
        $objProduct = new Product();
        $products = $objProduct->bulkDeleteProduct($input);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

}
