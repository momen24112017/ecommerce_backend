<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Status;
use App\User;
use App\BrandSetting;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPassword;
use App\Mail\SendMail;

class UserController extends Controller
{
    public function RegisterByFacebook(Request $request){
        $arr = array();
        $input = $request->all();
        //check user exist
        $objUser = new User();
        $user = $objUser->getUserByFacebookId($input['facebook_id']);
        if($user != null){
            $arr['data'] = $input;
            $arr = Status::mergeStatus($arr,4016);
            return $arr;
        }
        //store user data
        $result = User::create($input);       
        if($result){
            $arr['data'] = $input;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }
    }

    public function LoginByFacebook(Request $request){
        $arr = array();
        $input = $request->all();
        //get user info by Facebook Id
        $objUser = new User();
        $user = $objUser->getUserByFacebookId($input['facebook_id']);
        //check if user exist
        if($user != null){
            $arr['data']= $user;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }else{
            $arr['data'] = $user;
            $arr = Status::mergeStatus($arr,4030);
            return $arr;
        }
    }

    public function RegisterByGoogle(Request $request){
         
        $arr = array();
        $input = $request->all();
        //check user exist
        $objUser = new User();
        $user = $objUser->getUserByGoogleEmail($input['email']);
        if($user != null){
            $arr['data'] = $input;
            $arr = Status::mergeStatus($arr,4016);
            return $arr;
        }
        //store user data
        $result = User::create($input);
        //send mail to active user
        $data = array(
            'name'    => $input['name'],
            'email'   => $input['email'],
        );
        $resultEmail = Mail::to($data['email'])->send(new SendMail($data));
        if($result){
            $arr['data'] = $input;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }
    }

    public function LoginByGoogle(Request $request){
        $arr = array();
        $input = $request->all();
        //get user info by Facebook Id
        $objUser = new User();
        $user = $objUser->getUserByGoogleEmail($input['email']);
        //check if user exist
        if($user != null){
            $arr['data']= $user;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }else{
            $arr['data'] = $user;
            $arr = Status::mergeStatus($arr,4030);
            return $arr;
        }
    }
    
    public function Register(Request $request){
        
        $arr= array();
        $input=$request->all();
        
        //check user exist
        $objUser = new User();
        $user = $objUser->getUser($input['email']);
        if($user != null){
            $arr['data'] = $input;
            $arr = Status::mergeStatus($arr,4016);
            return $arr;
        }

        //confirm password
        if($input['password'] != $input['confirm_password']){
            $arr['data'] = $input;
            $arr = Status::mergeStatus($arr,5013);
            return $arr;
        }
        
        //hash password & make hashed password input
        $pass=$input['password'];
        $password = Hash::make($pass);
        $input['password']= $password;
        
        //store user data
        $result = User::create($input);
        
        //send mail to active user
        $data = array(
            'name'    => $input['name'],
            'email'   => $input['email'],
        );
        $resultEmail = Mail::to($data['email'])->send(new SendMail($data));
        
        if($result){
            $arr['data'] = $data;
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }
    }

    public function Login(Request $request){
        
        $arr= array();
        $email = $request->email;
        $password = $request->password;

        //get user info by email
        $objUser = new User();
        $user = $objUser->getUser($email);
        
        //check if user exist
        if($user != null){
            
            //check password
            if (Hash::check($password, $user['password'])){   
                $userInfo = $objUser->getUserByEmail($email);
                $arr['data']= $userInfo;
                $arr = Status::mergeStatus($arr,200);
                return $arr;
            }
            $arr['data']['email'] = $email;
            $arr['data']['password'] = $password;
            $arr = Status::mergeStatus($arr,5013);
            return $arr;
        }
        else{
            $arr['data'] = $user;
            $arr = Status::mergeStatus($arr,4030);
            return $arr;
        }
    }

    public function ActiveUser($email){
        $objUser = new User();
        $user = $objUser->activeUser($email);
        $arr['data'] = $user;
        return redirect('http://marouf.leatherworks.giraffecode.com/');
    }

    public function ResendConformationEmail($email){
        $objUser = new User();
        $user = $objUser->getUserByEmail($email);
        $data = array(
            'name'    => $user['name'],
            'email'   => $user['email'],
        ); 
        $resultEmail = Mail::to($data['email'])->send(new SendMail($data));
        $arr['data'] = $data;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function CheckEmail(Request $request){
        $arr = array();
        $objUser = new User();
        $user = $objUser->getUser($request['email']);
        if($user){
            $data = array(
                'id'      => $user['id'],
                'name'    => $user['name'],
                'email'   => $user['email'],
            );
            $resultEmail = Mail::to($data['email'])->send(new ForgotPassword($data));
            $arr['data'] =  $user['email'];
            $arr = Status::mergeStatus($arr,200);
            return $arr;
        }
        $arr = Status::mergeStatus($arr,5017);
        return $arr;
    }

    public function ResetPassword(Request $request, $email){
        $arr = array();
        $objUser = new User();
        $input = $request->all();
        $user = $objUser->getUser($email);
        if($input['password'] != $input['confirm_password']){
            $arr['password'] = $input['password'];
            $arr['confirm_password'] = $input['confirm_password'];
            $arr = Status::mergeStatus($arr,5013);
            return $arr;
        }
        $password = Hash::make($input['password']);
        $result = $objUser->updatePassword($user['id'], $password);
        $arr['data'] = $input['password'];
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }
    
    public function ResendMailforResetPassword($email){
        $arr = array();
        $objUser = new User();
        $user = $objUser->getUser($email);
        $data = array(
            'id'      => $user['id'],
            'name'    => $user['name'],
            'email'   => $user['email'],
        );
        $resultEmail = Mail::to($data['email'])->send(new ForgotPassword($data));
        $arr['data'] =  $user['email'];
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function addtoCartCheck($user_id){
        $arr= array();
        $objUser = new User();
        $user = $objUser->checkActive($user_id);
        $arr['data'] =  $user;
        // $arr = Status::mergeStatus($arr,5013);
        return $arr;
    }

    public function UpdateUserDate(Request $request){
        $arr = array();
        $input = $request->all();
        $userData = array(
            'id'        =>  $input['id'],
            'name'      =>  $input['name'],
            'address'   =>  $input['address'],
            'city'      =>  $input['city'],
            'phone'     =>  $input['phone'],
        );
        $objUser = new User();
        $userInfo = $objUser->saveUserData($userData);
        $arr['data'] =  $userData;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function GetBrandSetting(){
        $arr = array();
        $objBrandSetting = new BrandSetting();
        $brandSetting = $objBrandSetting->getBrandSetting();
        $arr['data'] = $brandSetting;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListBrandSetting(){
        $arr = array();
        $objBrandSetting = new BrandSetting();
        $brandSetting = $objBrandSetting->listBrandSetting();
        $arr['data'] = $brandSetting;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function UpdateBrandSetting(Request $request){
        $arr= array();
        $input = $request->all();
        $objBrandSetting = new BrandSetting();
        $brandSetting = $objBrandSetting->updateBrandSetting($input);
        $arr['data']= $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function SendNotification(Request $request){

        $input = $request->all();

        $contents = array("en" => $input['contents']);
        $headings = array("en" => $input['headings']);
        $hashes_array = array();
        array_push($hashes_array, array(
            "id"    => "like-button",
            "text"  => "Like",
            "icon"  => "http://i.imgur.com/N8SN8ZS.png",
            "url"   => "http://marouf.leatherworks.giraffecode.com/#/"
        ));
        array_push($hashes_array, array(
            "id"    => "like-button-2",
            "text"  => "Like2",
            "icon"  => "http://i.imgur.com/N8SN8ZS.png",
            "url"   => "http://marouf.leatherworks.giraffecode.com/#/"
        ));
        $fields = array(
            'app_id'            => "c1c5b131-9605-4486-8dfb-b04cd855ee2d",
            'included_segments' => array('All'),
            'data'              => array("foo" => "bar"),
            'headings'          => $headings,
            'contents'          => $contents,
            'web_buttons'       => $hashes_array
        );
        $fields = json_encode($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://app.onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Basic YmMxZjI5MmItYzNjYy00ZjRhLTk5Y2YtNGY4MWI4MmQzNGJk'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response, true);
        $fields = json_decode($fields, true);
        $data = array(
            'JSON sent'     => $fields,
            'JSON received' => $response
        );
        $arr['data'] =  $data;
        $arr = Status::mergeStatus($arr,200);
        return redirect('http://marouf.leatherworks.giraffecode.com/public/admin/sendNotifications');
        
    }

    public function SendNotificationtoUser(Request $request){

        $input = $request->all();
        $objUser = new User();
        $userDeviceId = $objUser->getUserDeviceId($input['user_id']);
        
        $content = array("en"   => $input['contents']);
        $headings = array("en"  => $input['headings']);
        $fields = array(
            'app_id'             => "c1c5b131-9605-4486-8dfb-b04cd855ee2d",
            'include_player_ids' => array($userDeviceId[0]['device_id']),
            'data'               => array("foo" => "bar"),
            'contents'           => $content,
            'headings'           => $headings,
        );
        $fields = json_encode($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://app.onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Basic YmMxZjI5MmItYzNjYy00ZjRhLTk5Y2YtNGY4MWI4MmQzNGJk'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        
        $response = json_decode($response, true);
        $fields = json_decode($fields, true);
        $data = array(
            'JSON sent'     => $fields,
            'JSON received' => $response
        );
        $arr['data'] =  $data;
        $arr = Status::mergeStatus($arr,200);
        return redirect('http://marouf.leatherworks.giraffecode.com/public/admin/sendNotifications');
        
    }

    public function adminSendNotificationtoAllUsers(){
        $objUser = new User();
        $users = $objUser->getNormalUsers();
        return view('send_notification_to_all_users',compact('users'));
    }

    public function ListUsers(){
        $objUser = new User();
        $users = $objUser->listUsers();
        $arr['data'] = $users;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListNormalUsers(){
        $objUser = new User();
        $users = $objUser->listNormalUsers();
        $arr['data'] = $users;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function CreateAdmin(Request $request){
        $validatedData = $request->validate([
            'name'=>'required|max:55',
            'email'=>'email|required|unique:users',
            'password'=>'required|confirmed'
        ]);

        $validatedData['password'] = bcrypt($request->password);

        $user = User::create($validatedData);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response(['user'=> $user, 'access_token'=> $accessToken]);
    }


    public function LoginAdmin(Request $request){
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);
       
        if(!auth()->attempt($loginData)) {
            return response(['message'=>'Invalid credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user' => auth()->user(), 'access_token' => $accessToken]);
    }
      
}
