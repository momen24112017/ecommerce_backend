<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\ProductCategory;

class CategoryController extends Controller
{
    public function ListCategory(){
        $arr = array();
        $objCategory = new ProductCategory();
        $arrCategories = $objCategory->listCategory();
        $arr['data'] = $arrCategories;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListCategoryDashboard(){
        $arr = array();
        $objCategory = new ProductCategory();
        $arrCategories = $objCategory->listCategoryDashboard();
        $arr['data'] = $arrCategories;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function CreateCategory(Request $request){
        $arr = array();
        $input = $request->all();
        $image = $input['image'];
        $imageName = $image->getClientOriginalName();
        $image->move('storage/product-categories', $imageName);
        $input['image'] = "product-categories/".$imageName;
        $result = ProductCategory::create($input);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function DeleteCategory($category_id){
        $arr = array();
        $objProductCategory = new ProductCategory();
        $category = $objProductCategory->deleteCategory($category_id);
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function EditCategory($category_id){
        $arr = array();
        $objProductCategory = new ProductCategory();
        $category = $objProductCategory->editCategory($category_id);
        $arr['data'] = $category;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function UpdateCategory(Request $request, $category_id){
        $arr = array();
        $input = $request->all();
        $objProductCategory = new ProductCategory();
        $category = $objProductCategory->updateCategory($input, $category_id);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function BulkDeleteCategory(Request $request){
        $arr = array();
        $input = $request->all();
        $objProductCategory = new ProductCategory();
        $category = $objProductCategory->bulkDeleteCategory($input);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

}
