<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promocode;
use App\Status;

class PromoCodeController extends Controller
{
    public function CheckPromoCode(Request $request){
        $arr= array();
        $input = $request->all();
        $objPromocode = new Promocode();
        $promoCode = $objPromocode->checkPromocode($input['promocode']);
        if(count($promoCode)==0){      
            $arr = Status::mergeStatus($arr,5010);
        }else{
            $arr['data'] = $promoCode;
            $arr = Status::mergeStatus($arr,200);
        }    
        return $arr;
    }

    public function CreatePromoCode(Request $request){
        $arr = array();
        $input = $request->all();
        $objPromocode = new Promocode();
        $result = $objPromocode->createPromocode($input); 
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListPromoCode(){
        $arr = array();
        $objPromocode = new Promocode();
        $promocode = $objPromocode->getPromocodes(); 
        $arr['data'] = $promocode;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function EditPromoCode($promocode_id){
        $arr = array();
        $objPromocode = new Promocode();
        $promocode = $objPromocode->getPromocodebyId($promocode_id); 
        $arr['data'] = $promocode;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }
    
    public function UpdatePromoCode(Request $request, $promocode_id){
        $arr = array();
        $input = $request->all();
        $objPromocode = new Promocode();
        $promocode = $objPromocode->updatePromocode($promocode_id, $input); 
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function DeletePromoCode($promocode_id){
        $arr = array();
        $objPromocode = new Promocode();
        $promocode = $objPromocode->deletePromocode($promocode_id);
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function BulkDeletePromoCode(Request $request){
        $arr = array();
        $input = $request->all();
        $objPromocode = new Promocode();
        $promocode = $objPromocode->bulkDeletePromoCode($input);
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

}
