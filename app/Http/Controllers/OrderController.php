<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\Status;
use App\Order;
use App\User;
use App\OrderProduct;
use App\DeliveryFees;
use App\Product;
use App\Promocode;
use App\Mail\OrderMail;
use App\Mail\AdminMail;

class OrderController extends Controller
{
    public function CreateOrder(Request $request){
        
        $arr= array();
        $input = $request->all();

        // get order code
        $orderCode = IdGenerator::generate(['table' => 'orders', 'length' => 10, 'prefix' => date("Ymd")]);

        // save user data
        $userData = array(
            'id'        =>  $input['user_id'],
            'name'      =>  $input['name'],
            'address'   =>  $input['address'],
            'city'      =>  $input['city'],
            'phone'     =>  $input['phone'],
        );
        $objUser = new User();
        $userInfo = $objUser->saveUserData($userData);
        
        //save order in database
        if($input['promocode'] != "" && $input['promocode'] != null){
            $objPromocode = new Promocode();
            $promocode = $objPromocode->getPromocodebyPromoCode($input['promocode']);
        }
        else{
            $input['promocode'] = null;
        }
        $orderData = array(
            'user_id'           =>  $input['user_id'],
            'promocode_id'      =>  $promocode['id'],
            'total_price'       =>  $input['total'],
            'subtotal_price'    =>  $input['subtotal'],
            'delivery_fees'     =>  $input['delivery_fees'],
            'order_code'        =>  $orderCode,
        );
        $Order = Order::create($orderData);
        
        // create order products
        $objOrderProduct = new OrderProduct();
        $OrderProduct = $objOrderProduct->createOrderProduct($input['order_products'], $input['user_id'], $Order['id']);
         
        //send mail to user
        $data = array(
            'name'              =>  $input['name'],
            'email'             =>  $input['email'],
            'order'             =>  $input['order_products'],
            'total_price'       =>  $input['total'],
            'subtotal_price'    =>  $input['subtotal'],
            'delivery_fees'     =>  $input['delivery_fees'],
            'promocode'         =>  $input['promocode'],
            'order_code'        =>  $orderCode,
        );
        $userEmail = Mail::to($data['email'])->send(new OrderMail($data));
        $address = env('MAIL_FROM_ADDRESS');
        $AdminEmail = Mail::to($address)->send(new AdminMail($data));

        // check data with success message
        $arr['data']['Order'] = $Order;
        $arr['data']['OrderProduct'] = $OrderProduct;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
        
    }

    public function GetOrders($user_id){
        $arr= array();
        $objOrder = new Order();
        $orders = $objOrder->getOrders($user_id);
        $arr['data'] =  $orders;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListOrders(){
        $arr= array();
        $objOrder = new Order();
        $orders = $objOrder->listOrders();
        $objUser = new User();
        $objPromocode = new Promocode();
        for($i = 0; $i < count($orders); $i++){
            $user = $objUser->getUserById($orders[$i]['user_id']);
            $orders[$i]['user_id'] = $user;
            $promoCode = $objPromocode->getPromocodebyId($orders[$i]['promocode_id']);
            $orders[$i]['promocode_id']= $promoCode;
        }
        $arr['data'] =  $orders;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListDeliveryFees(){
        $arr = array();
        $objDeliveryFees = new DeliveryFees();
        $result = $objDeliveryFees->listDeliveryFees();
        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function ListDeliveryFeesDashboard(){
        $arr = array();
        $objDeliveryFees = new DeliveryFees();
        $result = $objDeliveryFees->listDeliveryFeesDashboard();
        $arr['data'] = $result;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function CreateDeliveryFees(Request $request){
        $arr = array();
        $input = $request->all();
        $result = DeliveryFees::create($input);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function EditDeliveryFees($deliveryfees_id){
        $arr = array();
        $objDeliveryFees = new DeliveryFees();
        $DeliveryFees = $objDeliveryFees->editDeliveryFees($deliveryfees_id);
        $arr['data'] = $DeliveryFees;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function UpdateDeliveryFees(Request $request, $deliveryfees_id){
        $arr = array();
        $input = $request->all();
        $objDeliveryFees = new DeliveryFees();
        $DeliveryFees = $objDeliveryFees->updateDeliveryFees($input, $deliveryfees_id);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function DeleteDeliveryFees($deliveryfees_id){
        $arr = array();
        $objDeliveryFees = new DeliveryFees();
        $DeliveryFees = $objDeliveryFees->deleteDeliveryFees($deliveryfees_id);
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

    public function BulkDeleteDeliveryFees(Request $request){
        $arr = array();
        $input = $request->all();
        $objDeliveryFees = new DeliveryFees();
        $DeliveryFees = $objDeliveryFees->bulkDeleteDeliveryFees($input);
        $arr['data'] = $input;
        $arr = Status::mergeStatus($arr,200);
        return $arr;
    }

}
