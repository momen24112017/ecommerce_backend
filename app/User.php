<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends \TCG\Voyager\Models\User
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'address','city','phone', 'is_active', 'device_id', 'facebook_id'
    ];

    public function getUser($email){
        $user= $this->where('email', $email)->first();
        return $user;
    }

    public function getUserById($id){
        $user= $this->where('id', $id)->first();
        return $user;
    }

    public function getUserByEmail($email){
        $user= $this->where('email', $email)->first();
        return $user;
    }

    public function checkActive($id){
        $active = $this->where('id', $id)->get('is_active');
        return $active;
    }

    public function activeUser($email){
        $result = $this->where('email',$email)->update(['is_active'=> 1]);
        return $result;
    }

    public function saveUserData($userData){
        $result = $this->where('id', $userData['id'])->update([
            'name' => $userData['name'], 'address' => $userData['address'], 'city' => $userData['city'], 'phone' => $userData['phone']
        ]);
        return $result;
    }

    public function updatePassword($id, $password){
        $result = $this->where('id',$id)->update(['password'=> $password]);
        return $result;
    }

    public function getNormalUsers(){
        $arrUsers = array();
        $normalUsers = $this->where('role_id', 2)->get();
        foreach($normalUsers as $key => $value){
            $arrUsers[$key] = array(
                'id'    =>  $value['id'],
                'name'  =>  $value['name'],
                'email' =>  $value['email']
            );
        }
        return $arrUsers;
    }

    public function getUserDeviceId($id){
        $result = $this->where('id',$id)->get('device_id');
        return $result;
    }

    public function getUserByFacebookId($facebook_id){
        $user = $this->where('facebook_id', $facebook_id)->first();
        return $user;
    }

    public function getUserByGoogleEmail($email){
        $user = $this->where('email', $email)->first();
        return $user;
    }

    public function listUsers(){
        $user = $this->get();
        return $user;
    }

    public function listNormalUsers(){
        $user = $this->where('role_id', 2)->get();
        return $user;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
